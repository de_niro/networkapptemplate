//
//  Localization.swift
//  NetworkAppTemplate
//
//  Created by Lev on 15/12/2018.
//  Copyright © 2018 Lev. All rights reserved.
//

import Foundation

enum Localization: String {
    var local: String { return localizedWithDefault(rawValue) }

    case deliveryListTitle
    case deliveryDetails
    case detailsAt

    case errorReload
    case unknownErrorTitle
    case emptyDataErrorTitle
    case unknownErrorMessage
    case emptyDataErrorMessage

    func localizedWithDefault(_ key: String) -> String {

        if let message = getLocalization(for: key, from: nil) {
            return message
        }

        let language = "en"
        let path = Bundle.main.path(forResource: language, ofType: "lproj")
        let bundle = Bundle(path: path!)

        if let forcedString = bundle?.localizedString(forKey: key, value: nil, table: nil) {
            return forcedString
        } else {
            return key
        }
    }

    private func getLocalization(for key: String, from table: String?) -> String? {

        let message: String
        if let table = table {
            message = Bundle.main.localizedString(forKey: key, value: nil, table: table)
        } else {
            message = NSLocalizedString(key, comment: "")
        }
        return message != key ? message : nil
    }
}
