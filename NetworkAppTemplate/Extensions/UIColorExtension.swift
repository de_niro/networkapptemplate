//
//  UIColor.swift
//  NetworkAppTemplate
//
//  Created by Lev on 15/12/2018.
//  Copyright © 2018 Lev. All rights reserved.
//

import UIKit

extension UIColor {
    static let darkWhiteColor = w(242)

    private static func rgb(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat, alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(red: red / 255, green: green / 255, blue: blue / 255, alpha: alpha)
    }

    private static func w(_ white: CGFloat, alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(white: white / 255, alpha: alpha)
    }

    func image(_ size: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { rendererContext in
            self.setFill()
            rendererContext.fill(CGRect(origin: .zero, size: size))
        }
    }
}
