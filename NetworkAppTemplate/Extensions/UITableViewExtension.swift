import UIKit

protocol CellRegistrable: class {
    static var reuseIdentifier: String { get }
}

extension CellRegistrable {
    static var reuseIdentifier: String { return String(describing: self) }
}

extension UITableView {
    func registerReusableCell<T: UITableViewCell>(_: T.Type) where T: CellRegistrable {
        register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    }

    func dequeueReusableCell<T: UITableViewCell>(indexPath: IndexPath) -> T where T: CellRegistrable {
        return dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }

    func registerReusableHeaderFooterView<T: UITableViewHeaderFooterView>(_: T.Type) where T: CellRegistrable {
        register(T.self, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
    }

    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>() -> T? where T: CellRegistrable {
        return dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier) as! T?
    }
}
