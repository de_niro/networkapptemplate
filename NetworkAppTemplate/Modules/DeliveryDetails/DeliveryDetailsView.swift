import UIKit
import PinLayout
import MapKit

private struct Constants {
    static let itemViewHeight: CGFloat = 100
}

protocol DeliveryDetailsViewType: AnyView {
    func presentModel(model: DeliveryDetailsViewModel)
    func setLocation(_ location: CLLocation)
}

struct DeliveryDetailsViewModel {
    let text: String
    let image: UIImage?
    let address: String
}

final class DeliveryDetailsView: UIView {

    private let mapView = MKMapView()
    private let itemView = DeliveryItemView()

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(mapView)
        addSubview(itemView)
        mapView.isUserInteractionEnabled = true

        adjustSubviews()
    }

    private func adjustSubviews() {
        backgroundColor = .white
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        mapView.pin.top().horizontally().aspectRatio(1)
        itemView.pin.below(of: mapView).horizontally().height(Constants.itemViewHeight)
    }

    func setLocation(_ location: CLLocation) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = location.coordinate
        mapView.showAnnotations([annotation], animated: false)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension DeliveryDetailsView: DeliveryDetailsViewType {
    func presentModel(model: DeliveryDetailsViewModel) {
        itemView.presentModel(model: DeliveryItemViewModel(text: model.text, address: model.address))
        itemView.presentImage(model.image)
    }
}
