import UIKit
import Swinject
import SwinjectAutoregistration

final class DeliveryDetailsAssembly {
    private let resolver: Resolver

    init(resolver: Resolver) {
        self.resolver = resolver
    }

    func module(delivery: DeliveryImageItem) -> UIViewController {
        let view = resolver.resolve(DeliveryDetailsViewType.self)!
        let interactor = resolver.resolve(DeliveryDetailsInteractorType.self)!
        let wireframe = resolver.resolve(DeliveryDetailsWireframeType.self)!
        let controller = DeliveryDetailsViewController(view: view, interactor: interactor, wireframe: wireframe, deliveryModel: delivery)
        return controller
    }
}

final class DeliveryDetailsDIAssembly: Assembly {
    func assemble(container: Container) {
        container.register(DeliveryDetailsAssembly.self) { DeliveryDetailsAssembly(resolver: $0) }
        container.autoregister(DeliveryDetailsViewType.self, initializer: DeliveryDetailsView.init)
        container.autoregister(DeliveryDetailsInteractorType.self, initializer: DeliveryDetailsInteractor.init)
        container.autoregister(DeliveryDetailsWireframeType.self, initializer: DeliveryDetailsWireframe.init)
    }
}
