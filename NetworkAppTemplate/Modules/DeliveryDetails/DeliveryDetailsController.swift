import Foundation
import UIKit
import MapKit

final class DeliveryDetailsViewController: UIViewController {
    private let moduleView: DeliveryDetailsViewType
    private let interactor: DeliveryDetailsInteractorType
    private let wireframe: DeliveryDetailsWireframeType
    private let deliveryModel: DeliveryImageItem

    init(view: DeliveryDetailsViewType, interactor: DeliveryDetailsInteractorType, wireframe: DeliveryDetailsWireframeType, deliveryModel: DeliveryImageItem) {
        self.moduleView = view
        self.interactor = interactor
        self.wireframe = wireframe
        self.deliveryModel = deliveryModel

        super.init(nibName: nil, bundle: nil)

        moduleView.setLocation(CLLocation(latitude: deliveryModel.delivery.location.lat,
                                          longitude: deliveryModel.delivery.location.lng))
        moduleView.presentModel(model: DeliveryDetailsViewModel(text: deliveryModel.delivery.description,
                                                                image: deliveryModel.image,
                                                                address: deliveryModel.delivery.location.address))
    }

    override func loadView() {
        view = moduleView.view
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = Localization.deliveryDetails.local
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
