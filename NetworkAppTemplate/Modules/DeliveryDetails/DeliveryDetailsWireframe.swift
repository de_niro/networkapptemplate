import UIKit

protocol DeliveryDetailsWireframeType: WireframeType {
    
}

final class DeliveryDetailsWireframe: Wireframe, DeliveryDetailsWireframeType {


}
