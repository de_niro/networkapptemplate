import UIKit
import Swinject
import SwinjectAutoregistration

final class DeliveryListAssembly {
    private let resolver: Resolver

    init(resolver: Resolver) {
        self.resolver = resolver
    }

    func module() -> UIViewController {
        let view = resolver.resolve(DeliveryListViewType.self)!
        let interactor = resolver.resolve(DeliveryListInteractorType.self)!
        let wireframe = resolver.resolve(DeliveryListWireframeType.self)!

        let controller = DeliveryListViewController(view: view, interactor: interactor, wireframe: wireframe)
        wireframe.sourceViewController = controller
        return controller
    }
}

final class DeliveryListDIAssembly: Assembly {
    func assemble(container: Container) {
        container.register(DeliveryListAssembly.self) { DeliveryListAssembly(resolver: $0) }
        container.autoregister(DeliveryListViewType.self, initializer: DeliveryListView.init)
        container.register(DeliveryListInteractorType.self) { resolver in
            DeliveryListInteractor(networkService: resolver.resolve(NetworkServiceType.self)!,
                                   imageService: resolver.resolve(ImageServiceType.self)!)
        }
        container.autoregister(DeliveryListWireframeType.self, initializer: DeliveryListWireframe.init)
    }
}
