import UIKit

protocol DeliveryListInteractorType: class {
    func getDeliveries(_ paging: Paging, completion: @escaping AsyncResult<[Delivery], NetworkError>.Completion)
    func image(_ url: String, completion: @escaping AsyncResult<UIImage, NetworkError>.Completion)
}

final class DeliveryListInteractor: DeliveryListInteractorType {

    private let networkService: NetworkServiceType
    private let imageService: ImageServiceType

    init(networkService: NetworkServiceType, imageService: ImageServiceType) {
        self.networkService = networkService
        self.imageService = imageService
    }

    func getDeliveries(_ paging: Paging, completion: @escaping AsyncResult<[Delivery], NetworkError>.Completion) {
        networkService.getDeliveries(paging, completion: completion)
    }

    func image(_ url: String, completion: @escaping AsyncResult<UIImage, NetworkError>.Completion) {
        imageService.image(url, completion: completion)
    }
}
