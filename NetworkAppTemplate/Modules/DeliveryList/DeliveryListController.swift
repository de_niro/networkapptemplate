import Foundation
import UIKit

private struct Constants {
    static let limit: Int = 20
}

final class DeliveryListViewController: BaseViewController {
    private let moduleView: DeliveryListViewType
    private let interactor: DeliveryListInteractorType
    private let wireframe: DeliveryListWireframeType
    private var currentOffset = 0

    init(view: DeliveryListViewType, interactor: DeliveryListInteractorType, wireframe: DeliveryListWireframeType) {
        self.moduleView = view
        self.interactor = interactor
        self.wireframe = wireframe

        super.init(nibName: nil, bundle: nil)
    }

    override func loadView() {
        view = moduleView.view
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = Localization.deliveryListTitle.local

        getDeliveries()
        
        moduleView.onSelect = { [weak self] delivery in
            self?.wireframe.openDeliveryDetails(delivery: delivery)
        }

        moduleView.onLoadMore = { [weak self] in
            self?.getDeliveries()
        }
    }

    private func getDeliveries() {
        if moduleView.state == .normal {
            moduleView.set(state: .loading)
            interactor.getDeliveries(Paging(offset: String(currentOffset), limit: String(Constants.limit))) { [weak self] result in
                switch result {
                case let .success(deliveries):
                    if !deliveries.isEmpty {
                        var deliveryItems = [DeliveryLoaderItem]()
                        deliveries.forEach { delivery in
                            let deliveryLoader: DeliveryImageLoader = { [weak self] completion in
                                self?.interactor.image(delivery.imageURL) { result in
                                    switch result {
                                    case let .success(image):
                                        completion(image)
                                    case .failure: break
                                    }
                                }
                            }
                            deliveryItems.append(DeliveryLoaderItem(delivery: delivery, deliveryLoader: deliveryLoader))
                        }
                        self?.moduleView.presentModel(model: DeliveryListViewModel(deliveries: deliveryItems))
                        self?.currentOffset += Constants.limit
                    }
                    self?.moduleView.set(state: .normal)
                case let .failure(error):
                    self?.moduleView.set(state: .normal)
                    self?.showError(error.localizedTitle, message: error.localizedDescription) { [weak self] in
                        self?.getDeliveries()
                    }
                }
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
