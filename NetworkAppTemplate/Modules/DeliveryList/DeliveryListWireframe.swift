import UIKit

protocol DeliveryListWireframeType: WireframeType {
    func openDeliveryDetails(delivery: DeliveryImageItem)
}

final class DeliveryListWireframe: Wireframe, DeliveryListWireframeType {
    private let deliveryDetailsAssembly: DeliveryDetailsAssembly

    init(deliveryDetailsAssembly: DeliveryDetailsAssembly) {
        self.deliveryDetailsAssembly = deliveryDetailsAssembly
    }

    func openDeliveryDetails(delivery: DeliveryImageItem) {
        let module = deliveryDetailsAssembly.module(delivery: delivery)
        sourceViewController?.navigationController?.pushViewController(module, animated: true)
    }
}
