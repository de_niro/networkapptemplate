import UIKit
import PinLayout

private struct Constants {
    static let titleImageViewSideLength: CGFloat = 90
}

struct DeliveryListTableViewCellViewModel {
    let text: String
    let address: String
}

final class DeliveryListTableViewCell: UITableViewCell, CellRegistrable {

    let itemView = DeliveryItemView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(itemView)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        itemView.pin.all()
    }

    func presentModel(model: DeliveryListTableViewCellViewModel) {
        itemView.presentImage(UIColor.darkWhiteColor.image(CGSize(width: Constants.titleImageViewSideLength,
                                                                  height: Constants.titleImageViewSideLength)))
        itemView.presentModel(model: DeliveryItemViewModel(text: model.text, address: model.address))
    }

    func presentImage(_ image: UIImage?) {
        itemView.presentImage(image)
    }

    required init?(coder _: NSCoder) {
        fatalError()
    }
}
