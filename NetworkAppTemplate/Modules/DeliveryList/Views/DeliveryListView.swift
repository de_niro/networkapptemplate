import UIKit
import PinLayout

private struct Constants {
    static let tableViewHeight: CGFloat = 100
    static let tableHeaderViewHeight: CGFloat = 5
    static let tableFooterViewHeight: CGFloat = 100
}

protocol DeliveryListViewType: AnyView, AsyncViewStateable {
    var onSelect: ((DeliveryImageItem) -> ())? { get set }
    var onLoadMore: VoidHandler? { get set }
    var state: AsyncViewState { get }

    func presentModel(model: DeliveryListViewModel)
}

struct DeliveryImageItem {
    let delivery: Delivery
    let image: UIImage?
}

struct DeliveryLoaderItem {
    let delivery: Delivery
    let deliveryLoader: DeliveryImageLoader
}

struct DeliveryListViewModel {
    var deliveries: [DeliveryLoaderItem]
}

final class DeliveryListView: UIView {

    private var model: DeliveryListViewModel?
    private let tableView = UITableView()
    private let tableHeaderView = UIView()
    private let tableFooterView = UIView()
    private let activityView = UIView()
    private let activityIndicator = UIActivityIndicatorView()
    private let footerActivityIndicator = UIActivityIndicatorView()

    var onSelect: ((DeliveryImageItem) -> ())?
    var onLoadMore: VoidHandler?
    var state: AsyncViewState = .normal

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(tableView)
        addSubview(activityView)
        activityView.addSubview(activityIndicator)
        tableFooterView.addSubview(footerActivityIndicator)

        adjustSubviews()
    }

    private func adjustSubviews() {

        backgroundColor = .white
        activityView.backgroundColor = .white
        activityIndicator.color = .black
        activityIndicator.startAnimating()

        tableView.registerReusableCell(DeliveryListTableViewCell.self)
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.tableHeaderView = tableHeaderView

        footerActivityIndicator.color = .black
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        tableView.pin.all()
        activityView.pin.all()
        activityIndicator.pin.center()
        tableFooterView.frame.size = CGSize(width: tableView.frame.width, height: Constants.tableFooterViewHeight)
        footerActivityIndicator.pin.center()
        tableHeaderView.frame.size = CGSize(width: tableView.frame.width, height: Constants.tableHeaderViewHeight)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension DeliveryListView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model?.deliveries.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let model = model else { return UITableViewCell() }
        let cell: DeliveryListTableViewCell = tableView.dequeueReusableCell(indexPath: indexPath)
        cell.presentModel(model: DeliveryListTableViewCellViewModel(text: model.deliveries[indexPath.row].delivery.description,
                                                                    address: model.deliveries[indexPath.row].delivery.location.address))

        model.deliveries[indexPath.row].deliveryLoader { image in
            DispatchQueue.main.async {
                if let cellToUpdate = tableView.cellForRow(at: indexPath) as? DeliveryListTableViewCell {
                    cellToUpdate.presentImage(image)
                }
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.tableViewHeight
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let model = model,
            let cell = tableView.cellForRow(at: indexPath) as? DeliveryListTableViewCell else { return }
        onSelect?(DeliveryImageItem(delivery: model.deliveries[indexPath.row].delivery, image: cell.itemView.titleImageView.image))
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let count = model?.deliveries.count else { return }
        if indexPath.row == count - 1 {
            onLoadMore?()
        }
    }
}

extension DeliveryListView: DeliveryListViewType {
    func presentModel(model: DeliveryListViewModel) {
        if self.model == nil {
            self.model = model
        } else {
            self.model?.deliveries.append(contentsOf: model.deliveries)
        }
        tableView.reloadData()
    }
}

extension DeliveryListView: AsyncViewStateable {
    func set(state: AsyncViewState) {
        self.state = state
        switch state {
        case .loading:
            tableView.tableFooterView = tableFooterView
            footerActivityIndicator.startAnimating()
        case .normal:
            if activityView.isDescendant(of: self) {
                UIView.animate(withDuration: 0.3, animations: {
                    self.activityView.alpha = 0
                }) { _ in
                    self.activityView.removeFromSuperview()
                }
            }
            tableView.tableFooterView = nil
            footerActivityIndicator.stopAnimating()
        }
    }
}
