//
//  DependencyConfigurator.swift
//  NetworkAppTemplate
//
//  Created by Lev on 14/12/2018.
//  Copyright © 2018 Lev. All rights reserved.
//

import Swinject

final class DependencyConfigurator {
    private var assembler: Assembler

    var resolver: Resolver { return assembler.resolver }

    static let shared = DependencyConfigurator()

    private init() {
        assembler = Assembler([
            DeliveryListDIAssembly(),
            DeliveryDetailsDIAssembly(),
            ServiceAssembly()
            ])
    }
}
