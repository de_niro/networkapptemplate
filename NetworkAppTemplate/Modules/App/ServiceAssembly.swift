//
//  ServiceAssembly.swift
//  NetworkAppTemplate
//
//  Created by Lev on 14/12/2018.
//  Copyright © 2018 Lev. All rights reserved.
//

import Swinject
import Kingfisher

final class ServiceAssembly: Assembly {
    func assemble(container: Container) {
        container.autoregister(NetworkServiceType.self, initializer: NetworkService.init).inObjectScope(.container)
        container.autoregister(ImageServiceType.self, initializer: ImageService.init).inObjectScope(.container)

        container.register(KingfisherManager.self) { _ in
            KingfisherManager.shared
        }

        container.register(ImageServiceType.self) { resolver in
            ImageService(downloader: resolver.resolve(KingfisherManager.self)!)
        }
    }
}
