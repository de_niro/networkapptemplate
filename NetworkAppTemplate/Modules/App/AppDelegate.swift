//
//  AppDelegate.swift
//  NetworkAppTemplate
//
//  Created by Lev on 13/12/2018.
//  Copyright © 2018 Lev. All rights reserved.
//

import UIKit
import Swinject

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let resolver = DependencyConfigurator.shared.resolver
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.makeKeyAndVisible()
        self.window = window

        let rootViewController = UINavigationController(rootViewController: DeliveryListAssembly(resolver: resolver).module())
        window.rootViewController = rootViewController

        return true
    }
}
