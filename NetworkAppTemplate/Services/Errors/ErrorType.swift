//
//  ErrorType.swift
//  NetworkAppTemplate
//
//  Created by Lev on 15/12/2018.
//  Copyright © 2018 Lev. All rights reserved.
//

protocol ErrorType: Error {
    var localizedTitle: String { get }
    var localizedDescription: String? { get }
}
