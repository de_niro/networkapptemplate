//
//  NetworkError.swift
//  NetworkAppTemplate
//
//  Created by Lev on 14/12/2018.
//  Copyright © 2018 Lev. All rights reserved.
//

import Foundation

enum NetworkError: ErrorType {
    case error(NSError)
    case unknownError
    case emptyData

    var localizedTitle: String {
        switch self {
        case .unknownError: return Localization.unknownErrorTitle.local
        case .emptyData: return Localization.emptyDataErrorTitle.local
        case let .error(error): return error.localizedDescription
        }
    }

    var localizedDescription: String? {
        switch self {
        case .unknownError: return Localization.unknownErrorMessage.local
        case .emptyData: return Localization.emptyDataErrorMessage.local
        case .error: return nil
        }
    }
}
