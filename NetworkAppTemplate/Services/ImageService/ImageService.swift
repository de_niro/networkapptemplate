import Kingfisher

protocol ImageServiceType: class {
    func image(_ url: String, completion: @escaping AsyncResult<UIImage, NetworkError>.Completion)
}

final class ImageService: ImageServiceType {
    private let downloader: KingfisherManager

    init(downloader: KingfisherManager) {
        self.downloader = downloader

        downloader.cache.maxCachePeriodInSecond = TimeInterval(60 * 60 * 2)
        downloader.cache.maxDiskCacheSize = UInt(50 * 1024 * 1024)
    }

    func image(_ url: String, completion: @escaping AsyncResult<UIImage, NetworkError>.Completion) {
        if let url = URL(string: url) {
            image(url, completion: completion)
        }
    }

    func image(_ url: URL, completion: @escaping AsyncResult<UIImage, NetworkError>.Completion) {
        let resource = ImageResource(downloadURL: url)

        downloader.retrieveImage(with: resource, options: nil, progressBlock: nil) { image, error, _, _ in
            if let image = image {
                completion(.success(image))
            } else if let error = error {
                completion(.failure(.error(error)))
            } else {
                completion(.failure(.unknownError))
            }
        }
    }
}
