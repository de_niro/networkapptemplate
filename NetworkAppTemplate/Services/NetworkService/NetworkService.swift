//
//  NetworkService.swift
//  NetworkAppTemplate
//
//  Created by Lev on 14/12/2018.
//  Copyright © 2018 Lev. All rights reserved.
//

import Alamofire

enum HttpMethod: String {
    case get
    case post
}

struct HttpHeadersValue {
    static let json = "application/json"
}

struct HttpHeadersType {
    static let contentType = "Content-Type"
}

struct HttpParameters {
    static let offset = "offset"
    static let limit = "limit"
}

struct Paging {
    let offset: String
    let limit: String
}

private struct Constants {
    static let timeout: TimeInterval = 10
}

protocol NetworkServiceType: class {
    func getDeliveries(_ paging: Paging, completion: @escaping AsyncResult<Deliveries, NetworkError>.Completion)
}

final class NetworkService: NetworkServiceType {

    private var manager: SessionManager?

    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = Constants.timeout
        configuration.timeoutIntervalForResource = Constants.timeout
        manager = Alamofire.SessionManager(configuration: configuration)
    }

    func getDeliveries(_ paging: Paging, completion: @escaping AsyncResult<Deliveries, NetworkError>.Completion) {
        let headers: HTTPHeaders = [HttpHeadersType.contentType: HttpHeadersValue.json]
        let parameters: Parameters = [HttpParameters.offset: paging.offset,
                                      HttpParameters.limit: paging.limit]

        func decodeData(_ data: Data?, completionError: NetworkError) {
            do {
                guard let data = data else {
                    completion(.failure(completionError))
                    return
                }
                let model = try JSONDecoder().decode(Deliveries.self, from: data)
                completion(.success(model))
            } catch {
                completion(.failure(.error(error as NSError)))
            }
        }

        do {
            guard let manager = manager,
                let url = try queryParameterEncodedRequestURL(urlString: Links.deliveriesUrlString, parameters: parameters) else {
                    completion(.failure(.unknownError))
                    return
            }

            manager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { [weak self] response in
                switch (response.result) {
                case .success:
                    decodeData(response.data, completionError: .emptyData)
                case .failure:
                    decodeData(self?.cachedResponseData(url), completionError: response.error != nil ? .error(response.error! as NSError) : .unknownError)
                }
            }
        } catch let error as NSError {
            completion(.failure(.error(error)))
        }
    }

    private func cachedResponseData(_ url: URL) -> Data? {
        var request = URLRequest(url: url)
        request.httpMethod = HttpMethod.get.rawValue
        request.setValue(HttpHeadersValue.json, forHTTPHeaderField: HttpHeadersType.contentType)
        request.cachePolicy = .returnCacheDataElseLoad
        return URLCache.shared.cachedResponse(for: request)?.data
    }

    private func queryParameterEncodedRequestURL(urlString: String, parameters: Parameters) throws -> URL? {
        guard let url = URL(string: urlString) else { return nil }
        let urlRequest = URLRequest(url: url)
        var encodedURLRequest: URLRequest?
        encodedURLRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)
        return encodedURLRequest?.url
    }
}
