//
//  Delivery.swift
//  NetworkAppTemplate
//
//  Created by Lev on 14/12/2018.
//  Copyright © 2018 Lev. All rights reserved.
//

import UIKit
typealias Deliveries = [Delivery]

struct Delivery: Codable {
    let id: Int
    let description: String
    let imageURL: String
    let location: Location

    enum CodingKeys: String, CodingKey {
        case id, description
        case imageURL = "imageUrl"
        case location
    }
}

struct Location: Codable {
    let lat, lng: Double
    let address: String
}
