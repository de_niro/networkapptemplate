//
//  AsyncResult.swift
//  NetworkAppTemplate
//
//  Created by Lev on 14/12/2018.
//  Copyright © 2018 Lev. All rights reserved.
//

enum AsyncResult<T, E> {
    case success(T)
    case failure(E)

    typealias Completion = (AsyncResult) -> Void
}
