protocol AsyncViewStateable: class {
    func set(state: AsyncViewState)
}

enum AsyncViewState {
    case loading
    case normal
}
