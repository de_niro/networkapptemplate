//
//  BaseViewController.swift
//  NetworkAppTemplate
//
//  Created by Lev on 15/12/2018.
//  Copyright © 2018 Lev. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    func showError(_ title: String, message: String? = nil, completion: VoidHandler? = nil) {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: Localization.errorReload.local, style: .default, handler: { _ in completion?() }))
        present(controller, animated: true, completion: nil)
    }
}
