//
//  Wireframe.swift
//  NetworkAppTemplate
//
//  Created by Lev on 14/12/2018.
//  Copyright © 2018 Lev. All rights reserved.
//

import UIKit

protocol WireframeType: class {
    var sourceViewController: UIViewController? { get set }
    var closeCompletion: VoidHandler? { get set }
}

class Wireframe: WireframeType {
    weak var sourceViewController: UIViewController?
    var closeCompletion: VoidHandler?
}
