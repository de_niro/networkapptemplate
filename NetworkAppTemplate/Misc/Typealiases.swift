//
//  Typealiases.swift
//  NetworkAppTemplate
//
//  Created by Lev on 14/12/2018.
//  Copyright © 2018 Lev. All rights reserved.
//

import UIKit

typealias VoidHandler = (() -> Void)
typealias DeliveryImageLoader = (@escaping (UIImage?) -> Void) -> Void
