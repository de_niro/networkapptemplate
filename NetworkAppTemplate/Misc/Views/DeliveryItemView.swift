//
//  DeliveryItemView.swift
//  NetworkAppTemplate
//
//  Created by Lev on 15/12/2018.
//  Copyright © 2018 Lev. All rights reserved.
//

import PinLayout

private struct Constants {
    static let hMargin: CGFloat = 10
    static let titleImageViewSideLength: CGFloat = 90
}

struct DeliveryItemViewModel {
    let text: String
    let address: String
}

final class DeliveryItemView: UIView {

    private let titleLabel = UILabel()
    let titleImageView = UIImageView()

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(titleLabel)
        addSubview(titleImageView)

        adjustSubviews()
    }

    private func adjustSubviews() {
        titleLabel.numberOfLines = 0
        titleImageView.contentMode = .scaleAspectFill
        titleImageView.clipsToBounds = true
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        titleImageView.pin.left(Constants.hMargin).vCenter().size(Constants.titleImageViewSideLength)
        titleLabel.pin.after(of: titleImageView, aligned: .center).marginLeft(Constants.hMargin).sizeToFit(.width).right(Constants.hMargin)
    }

    func presentModel(model: DeliveryItemViewModel) {
        titleLabel.text = String(format: Localization.detailsAt.local, model.text, model.address)

        setNeedsLayout()
        layoutIfNeeded()
    }

    func presentImage(_ image: UIImage?) {
        titleImageView.image = image
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
