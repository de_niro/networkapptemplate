//
//  AnyView.swift
//  NetworkAppTemplate
//
//  Created by Lev on 14/12/2018.
//  Copyright © 2018 Lev. All rights reserved.
//

import UIKit

protocol AnyView: class {
    var view: UIView { get }
}

extension AnyView where Self: UIView {
    var view: UIView { return self }
}
